var resultado

const calc = (stringCal) => {
  stringCal ? Verificacion(stringCal) : resultado = 0

  return resultado
}

function Verificacion (valor) {
  if (!isNaN(valor)) {
    let numero = verifacionDelTamaño(valor)

    resultado = verificationContainNumberNegative([numero]) ? `Negativos no soportados -> (${numero})` : numero
  } else {
    let delimitador = false
    let Linea = false
    let nuevoValor

    Linea = contieneSaltoDeLinea(valor)
    Linea ? nuevoValor = removerSaltoDeLinea(valor) : Linea = false

    delimitador = contieneDelimitador(valor)
    delimitador ? nuevoValor = obtenerElValorDelDelimitador(valor) : delimitador = false
    Linea === false && delimitador === false ? nuevoValor = valor : valor = ''

    if (nuevoValor) {
      generarElCalculo(nuevoValor)
    } else {
      resultado = nuevoValor
    }
  }
}

function generarElCalculo (valor) {
  let generarSuma = 0
  let esNegativo = false
  let Numero
  let traslateInArray = separateString(valor, ',')

  for (let i = 0; i < traslateInArray.length; i++) {
    Numero = !isNaN(traslateInArray[i])
    if (Numero) {
      let tamañoDelNumero = verifacionDelTamaño(traslateInArray[i])
      let contieneNegativo = verificationContainNumberNegative([ tamañoDelNumero ])

      if (!contieneNegativo) {
        generarSuma += tamañoDelNumero
      } else {
        esNegativo = true
        break
      }
    }
  }

  esNegativo ? resultado = `Negativos no soportados -> (${valor})` : resultado = generarSuma
}

function verifacionDelTamaño (valor) {
  return Number(valor) < 1000 ? Number(valor) : 0
}

function verificationContainNumberNegative (valueArray) {
  let respuesta
  let tieneNegativo = []

  valueArray.forEach(element => {
    element > -1 ? tieneNegativo.push(false) : tieneNegativo.push(true)
  })

  for (let i = 0; i < tieneNegativo.length; i++) {
    tieneNegativo[i] ? respuesta = true : respuesta = false
  }

  return respuesta
}

function separateString (valor, separator) {
  return valor.split(separator)
}

function contieneSaltoDeLinea (valor) {
  let tieneLinea = false
  let Arr = valor.split('\n')

  if (Arr.length > 1) {
    tieneLinea = true
  }

  return tieneLinea
}

function removerSaltoDeLinea (valor) {
  let numeroDelString = ''
  let removedIntroLine
  let valorDelArreglo

  removedIntroLine = valor.split('\n').join(',')
  valorDelArreglo = removedIntroLine.split(',')

  for (let i = 0; i < valorDelArreglo.length; i++) {
    if (!isNaN(valorDelArreglo[i])) {
      numeroDelString = numeroDelString + ',' + valorDelArreglo[i]
    }
  }
  return numeroDelString.slice(1)
}

function contieneDelimitador (valor) {
  let respuesta = false
  let delimitador = valor[0] + valor[1]

  if (delimitador === '//') {
    respuesta = true
  }

  return respuesta
}

function obtenerElValorDelDelimitador (valor) {
  let Linea = valor.search('\n')
  let data = valor.slice(Linea + 1)
  let delimitator = valor.slice(2, Linea)
  let generarElString = data

  for (let i = 0; i < delimitator.length; i++) {
    generarElString = generarElString.split(delimitator[i]).join(',')
  }

  return generarElString
}

module.exports = calc

/* eslint-disable no-undef */
const calculator = require('../app')

test('Espacios en blanco retorna 0', () => {
  let send = ''
  let toBe = 0
  expect(calculator(send)).toBe(toBe)
})

test('El valor 20 retorna 20', () => {
  let send = '20'
  let toBe = 20
  expect(calculator(send)).toBe(toBe)
})

test('La suma de 100 + 10 retorna -> 110', () => {
  let send = '100,10'
  let toBe = 110
  expect(calculator(send)).toBe(toBe)
})

test('Suma 5 + 4000 retornaria 5', () => {
  let send = '5,4000'
  let toBe = 5
  expect(calculator(send)).toBe(toBe)
})

test('Suma 5 + (-500) retornaria -> "Negativos no soportados -> (5, -500)"', () => {
  let send = '5,-500'
  let toBe = 'Negativos no soportados -> (5,-500)'
  expect(calculator(send)).toEqual(toBe)
})

test('La cadena 1/n2,4 retornaria -> 7', () => {
  let send = '1\n2,4'
  let toBe = 7
  expect(calculator(send)).toBe(toBe)
})

test('La cadena //;/n1;2 retornaria -> 3', () => {
  let send = '//;\n1;2'
  let toBe = 3
  expect(calculator(send)).toBe(toBe)
})

test('La cadena //***/n1***2***3 retornaria -> 10', () => {
  let send = '//***\n5***2***3'
  let toBe = 10
  expect(calculator(send)).toBe(toBe)
})

test('La cadena //*%/n1*2%5 retornaria -> 8', () => {
  let send = '//*%\n1*2%5'
  let toBe = 8
  expect(calculator(send)).toBe(toBe)
})
